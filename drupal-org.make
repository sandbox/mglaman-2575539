api = 2
core = 8.x

defaults[projects][subdir] = contrib

projects[honeypot][version] = 1.18-beta5
projects[token][version] = 1.x
projects[pathauto][version] = 1.x
projects[pathautho][patch][2168193] = https://www.drupal.org/files/issues/2168193_pathauto_D8-port_38.patch
projects[bootstrap][version] = 3.x
