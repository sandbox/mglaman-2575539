api = 2
core = 8.x

includes[] = drupal-org-core.make

projects[drupal8ftw][type] = profile
projects[drupal8ftw][download][type] = git
projects[drupal8ftw][download][url] = mglaman@git.drupal.org:sandbox/mglaman/2575539.git
projects[drupal8ftw][download][branch] = 8.x-1.x
